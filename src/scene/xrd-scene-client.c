/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "xrd-scene-client.h"

#include "xrd-client-private.h"

#include "xrd-scene-window-private.h"
#include "xrd-scene-desktop-cursor.h"
#include "xrd-scene-pointer-tip.h"
#include "xrd-scene-renderer.h"
#include "xrd-scene-device-manager.h"
#include "xrd-scene-background.h"

struct _XrdSceneClient
{
  XrdClient parent;

  XrdSceneDeviceManager *device_manager;

  graphene_matrix_t mat_view[2];
  graphene_matrix_t mat_projection[2];

  float near;
  float far;

  gboolean have_projection;

  XrdSceneBackground *background;
};

G_DEFINE_TYPE (XrdSceneClient, xrd_scene_client, XRD_TYPE_CLIENT)

static void xrd_scene_client_finalize (GObject *gobject);

void _init_device_model (XrdSceneClient *self,
                         uint32_t        device_id);
void _init_device_models (XrdSceneClient *self);
void _update_device_poses (XrdSceneClient *self);
void _render_stereo (XrdSceneClient *self, VkCommandBuffer cmd_buffer);
static void
_device_activate_cb (GxrContext          *context,
                     GxrDeviceIndexEvent *event,
                     gpointer            _self);
static void
_device_deactivate_cb (GxrContext          *context,
                       GxrDeviceIndexEvent *event,
                       gpointer            _self);
static bool
_init_vulkan (XrdSceneClient   *self,
              XrdSceneRenderer *renderer,
              GulkanClient     *gc);

static void
xrd_scene_client_init (XrdSceneClient *self)
{
  xrd_client_set_upload_layout (XRD_CLIENT (self),
                                VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL);

  self->device_manager = xrd_scene_device_manager_new ();
  self->background = xrd_scene_background_new ();

  self->near = 0.05f;
  self->far = 100.0f;

  self->have_projection = FALSE;
}

XrdSceneClient *
xrd_scene_client_new (void)
{
  XrdSceneClient *self =
    (XrdSceneClient *) g_object_new (XRD_TYPE_SCENE_CLIENT, 0);

  GSList *instance_ext_list =
    gulkan_client_get_external_memory_instance_extensions ();

  GSList *device_ext_list =
    gulkan_client_get_external_memory_device_extensions ();

  GxrContext *context =
    gxr_context_new_from_vulkan_extensions (GXR_APP_SCENE,
                                            instance_ext_list,
                                            device_ext_list);

  g_slist_free (instance_ext_list);
  g_slist_free (device_ext_list);

  if (!context)
    {
      g_object_unref (self);
      g_error ("Could not init VR runtime.\n");
      return NULL;
    }

  xrd_client_set_context (XRD_CLIENT (self), context);

  GulkanClient *gc = gxr_context_get_gulkan (context);

  XrdSceneRenderer *renderer = xrd_scene_renderer_get_instance ();
  if (!_init_vulkan (self, renderer, gc))
    {
      g_object_unref (self);
      g_error ("Could not init Vulkan.\n");
      return NULL;
    }

  g_signal_connect (context, "device-activate-event",
                    (GCallback) _device_activate_cb, self);
  g_signal_connect (context, "device-deactivate-event",
                    (GCallback) _device_deactivate_cb, self);

  xrd_client_init_input_callbacks (XRD_CLIENT (self));

  return self;
}

static void
xrd_scene_client_finalize (GObject *gobject)
{
  XrdSceneClient *self = XRD_SCENE_CLIENT (gobject);

  g_clear_object (&self->device_manager);
  g_object_unref (self->background);

  G_OBJECT_CLASS (xrd_scene_client_parent_class)->finalize (gobject);

  xrd_scene_renderer_destroy_instance ();
}

static void
_device_activate_cb (GxrContext          *context,
                     GxrDeviceIndexEvent *event,
                     gpointer            _self)
{
  (void) context;
  XrdSceneClient *self = (XrdSceneClient*) _self;
  g_print ("Device %lu activated, initializing model.\n",
           event->controller_handle);
  _init_device_model (self, (uint32_t) event->controller_handle);
}

static void
_device_deactivate_cb (GxrContext          *context,
                       GxrDeviceIndexEvent *event,
                       gpointer            _self)
{
  (void) context;
  XrdSceneClient *self = (XrdSceneClient*) _self;
  g_print ("Device %lu deactivated. Removing scene device.\n",
           event->controller_handle);
  xrd_scene_device_manager_remove (self->device_manager,
                                   (uint32_t) event->controller_handle);
  /* TODO: Remove pointer in client */
  // g_hash_table_remove (self->pointers, &event->index);
}

static void
_render_pointers (XrdSceneClient    *self,
                  GxrEye             eye,
                  VkCommandBuffer    cmd_buffer,
                  VkPipeline        *pipelines,
                  VkPipelineLayout   pipeline_layout,
                  graphene_matrix_t *vp)
{
  GxrContext *context = xrd_client_get_gxr_context (XRD_CLIENT (self));
  if (!gxr_context_is_input_available (context))
    return;

  GList *controllers =
    g_hash_table_get_values (xrd_client_get_controllers (XRD_CLIENT (self)));
  for (GList *l = controllers; l; l = l->next)
    {
      XrdController *controller = XRD_CONTROLLER (l->data);

      XrdScenePointer *pointer =
        XRD_SCENE_POINTER (xrd_controller_get_pointer (controller));
      xrd_scene_pointer_render (pointer, eye,
                                pipelines[PIPELINE_POINTER],
                                pipelines[PIPELINE_SELECTION],
                                pipeline_layout, cmd_buffer, vp);
    }
  g_list_free (controllers);
}

/*
 * Since we are using world space positons for the lights, this only needs
 * to be run once for both eyes
 */
static void
_update_lights_cb (gpointer _self)
{
  XrdSceneClient *self = XRD_SCENE_CLIENT (_self);

  GList *controllers =
    g_hash_table_get_values (xrd_client_get_controllers (XRD_CLIENT (self)));

  XrdSceneRenderer *renderer = xrd_scene_renderer_get_instance ();
  xrd_scene_renderer_update_lights (renderer, controllers);

  g_list_free (controllers);
}

static void
_render_eye_cb (uint32_t         eye,
                VkCommandBuffer  cmd_buffer,
                VkPipelineLayout pipeline_layout,
                VkPipeline      *pipelines,
                gpointer         _self)
{
  XrdSceneClient *self = XRD_SCENE_CLIENT (_self);

  graphene_matrix_t vp;
  graphene_matrix_multiply (&self->mat_view[eye],
                            &self->mat_projection[eye], &vp);

  XrdWindowManager *manager = xrd_client_get_manager (XRD_CLIENT (self));

  xrd_scene_background_render (self->background, eye,
                               pipelines[PIPELINE_BACKGROUND],
                               pipeline_layout, cmd_buffer, &vp);

  for (GSList *l = xrd_window_manager_get_windows (manager);
       l != NULL; l = l->next)
    {
      xrd_scene_window_draw (XRD_SCENE_WINDOW (l->data), eye,
                             pipelines[PIPELINE_WINDOWS],
                             pipeline_layout,
                             cmd_buffer, &vp);
    }

  for (GSList *l = xrd_window_manager_get_buttons (manager);
       l != NULL; l = l->next)
    {
      xrd_scene_window_draw_shaded (XRD_SCENE_WINDOW (l->data), eye,
                                    pipelines[PIPELINE_WINDOWS],
                                    pipeline_layout,
                                    cmd_buffer, &self->mat_view[eye],
                                   &self->mat_projection[eye]);
    }

  _render_pointers (self, eye, cmd_buffer, pipelines, pipeline_layout, &vp);

  GxrContext *context = xrd_client_get_gxr_context (XRD_CLIENT (self));
  xrd_scene_device_manager_render (self->device_manager,
                                   gxr_context_is_input_available (context),
                                   eye, cmd_buffer,
                                   pipelines[PIPELINE_DEVICE_MODELS],
                                   pipeline_layout, &vp);

  GList *controllers =
    g_hash_table_get_values (xrd_client_get_controllers (XRD_CLIENT (self)));
  for (GList *l = controllers; l; l = l->next)
    {
      XrdController *controller = XRD_CONTROLLER (l->data);
      XrdScenePointerTip *scene_tip =
        XRD_SCENE_POINTER_TIP (xrd_controller_get_pointer_tip (controller));
      xrd_scene_window_draw (XRD_SCENE_WINDOW (scene_tip), eye,
                             pipelines[PIPELINE_TIP],
                             pipeline_layout,
                             cmd_buffer, &vp);
    }
  g_list_free (controllers);

  XrdDesktopCursor *cursor = xrd_client_get_desktop_cursor (XRD_CLIENT (self));
  XrdSceneDesktopCursor *scene_cursor = XRD_SCENE_DESKTOP_CURSOR (cursor);
  xrd_scene_window_draw (XRD_SCENE_WINDOW (scene_cursor), eye,
                         pipelines[PIPELINE_TIP],
                         pipeline_layout,
                         cmd_buffer, &vp);
}

static bool
_init_vulkan (XrdSceneClient   *self,
              XrdSceneRenderer *renderer,
              GulkanClient     *gc)
{
  GxrContext *context = xrd_client_get_gxr_context (XRD_CLIENT (self));
  if (!xrd_scene_renderer_init_vulkan (renderer, context))
    return false;

  _init_device_models (self);

  GulkanDevice *device = gulkan_client_get_device (gc);

  VkDescriptorSetLayout *descriptor_set_layout =
    xrd_scene_renderer_get_descriptor_set_layout (renderer);

  xrd_scene_background_initialize (self->background,
                                   device,
                                   descriptor_set_layout);

  XrdDesktopCursor *cursor =
    XRD_DESKTOP_CURSOR (xrd_scene_desktop_cursor_new ());
  xrd_client_set_desktop_cursor (XRD_CLIENT (self), cursor);

  vkQueueWaitIdle (gulkan_device_get_queue_handle (device));

  xrd_scene_renderer_set_render_cb (renderer, _render_eye_cb, self);
  xrd_scene_renderer_set_update_lights_cb (renderer, _update_lights_cb, self);

  return true;
}

static void
_init_controller (XrdClient     *client,
                  XrdController *controller)
{
  GxrContext *context = xrd_client_get_gxr_context (client);
  GulkanClient *gc = gxr_context_get_gulkan (context);
  GulkanDevice *device = gulkan_client_get_device (gc);

  XrdSceneRenderer *renderer = xrd_scene_renderer_get_instance ();

  VkDescriptorSetLayout *descriptor_set_layout =
    xrd_scene_renderer_get_descriptor_set_layout (renderer);

  XrdScenePointer *pointer = xrd_scene_pointer_new ();
  xrd_scene_pointer_initialize (pointer, device,
                                descriptor_set_layout);
  xrd_controller_set_pointer (controller, XRD_POINTER (pointer));

  XrdScenePointerTip *pointer_tip = xrd_scene_pointer_tip_new ();
  xrd_controller_set_pointer_tip (controller, XRD_POINTER_TIP (pointer_tip));
}

void
_init_device_model (XrdSceneClient *self,
                    uint32_t        device_id)
{
  GxrContext *context = xrd_client_get_gxr_context (XRD_CLIENT (self));

  XrdSceneRenderer *renderer = xrd_scene_renderer_get_instance ();
  VkDescriptorSetLayout *descriptor_set_layout =
    xrd_scene_renderer_get_descriptor_set_layout (renderer);

  gchar *model_name = gxr_context_get_device_model_name (context, device_id);
  bool is_controller = gxr_context_device_is_controller (context, device_id);

  xrd_scene_device_manager_add (self->device_manager, context,
                                device_id, model_name, is_controller,
                                descriptor_set_layout);

  g_free (model_name);
}

void
_init_device_models (XrdSceneClient *self)
{
  GxrContext *context = xrd_client_get_gxr_context (XRD_CLIENT (self));
  for (uint32_t i = GXR_DEVICE_INDEX_HMD + 1; i < GXR_DEVICE_INDEX_MAX; i++)
    {
      if (!gxr_context_is_tracked_device_connected (context, i))
        continue;
      _init_device_model (self, i);
    }
}

void
xrd_scene_client_render (XrdSceneClient *self)
{
  GxrContext *context = xrd_client_get_gxr_context (XRD_CLIENT (self));
  if (!gxr_context_begin_frame (context))
    return;

  for (uint32_t eye = 0; eye < 2; eye++)
    gxr_context_get_view (context, eye, &self->mat_view[eye]);

  if (!self->have_projection)
    {
      for (uint32_t eye = 0; eye < 2; eye++)
        gxr_context_get_projection (context, eye, self->near, self->far,
                                   &self->mat_projection[eye]);
      self->have_projection = TRUE;
    }

  XrdSceneRenderer *renderer = xrd_scene_renderer_get_instance ();
  xrd_scene_renderer_draw (renderer);

  GxrPose poses[GXR_DEVICE_INDEX_MAX];
  gxr_context_end_frame (context, poses);

  xrd_scene_device_manager_update_poses (self->device_manager, poses);
}

static GulkanClient *
_get_gulkan (XrdClient *client)
{
  GxrContext *context = xrd_client_get_gxr_context (client);
  return gxr_context_get_gulkan (context);
}

static XrdWindow *
_window_new_from_meters (XrdClient  *client,
                         const char *title,
                         float       width,
                         float       height,
                         float       ppm)
{
  (void) client;
  XrdSceneWindow *window = xrd_scene_window_new_from_meters (title, width,
                                                             height, ppm);
  return XRD_WINDOW (window);
}

static XrdWindow *
_window_new_from_data (XrdClient *client, XrdWindowData *data)
{
  (void) client;
  XrdSceneWindow *window = xrd_scene_window_new_from_data (data);
  return XRD_WINDOW (window);
}

static XrdWindow *
_window_new_from_pixels (XrdClient  *client,
                         const char *title,
                         uint32_t    width,
                         uint32_t    height,
                         float       ppm)
{
  (void) client;
  XrdSceneWindow *window = xrd_scene_window_new_from_pixels (title, width,
                                                             height, ppm);
  return XRD_WINDOW (window);
}

static XrdWindow *
_window_new_from_native (XrdClient   *client,
                         const gchar *title,
                         gpointer     native,
                         uint32_t     width_pixels,
                         uint32_t     height_pixels,
                         float        ppm)
{
  (void) client;
  XrdSceneWindow *window = xrd_scene_window_new_from_native (title, native,
                                                             width_pixels,
                                                             height_pixels,
                                                             ppm);
  return XRD_WINDOW (window);
}

static void
xrd_scene_client_class_init (XrdSceneClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = xrd_scene_client_finalize;

  XrdClientClass *xrd_client_class = XRD_CLIENT_CLASS (klass);
  xrd_client_class->get_gulkan = _get_gulkan;
  xrd_client_class->init_controller = _init_controller;

  xrd_client_class->window_new_from_meters = _window_new_from_meters;
  xrd_client_class->window_new_from_data = _window_new_from_data;
  xrd_client_class->window_new_from_pixels = _window_new_from_pixels;
  xrd_client_class->window_new_from_native = _window_new_from_native;
}
