/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include "xrd-overlay-client.h"

#include "xrd-client-private.h"
#include "xrd-overlay-window-private.h"

#include "xrd-overlay-desktop-cursor.h"
#include "xrd-overlay-pointer.h"
#include "xrd-overlay-pointer-tip.h"

struct _XrdOverlayClient
{
  XrdClient parent;

  gboolean pinned_only;
  XrdOverlayWindow *pinned_button;
};

G_DEFINE_TYPE (XrdOverlayClient, xrd_overlay_client, XRD_TYPE_CLIENT)

static void
xrd_overlay_client_finalize (GObject *gobject);

static void
xrd_overlay_client_init (XrdOverlayClient *self)
{
  xrd_client_set_upload_layout (XRD_CLIENT (self),
                                VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);

  self->pinned_only = FALSE;
}

XrdOverlayClient *
xrd_overlay_client_new (void)
{
  XrdOverlayClient *self =
    (XrdOverlayClient*) g_object_new (XRD_TYPE_OVERLAY_CLIENT, 0);

  GSList *instance_ext_list =
    gulkan_client_get_external_memory_instance_extensions ();

  GSList *device_ext_list =
    gulkan_client_get_external_memory_device_extensions ();

  GxrContext *context =
    gxr_context_new_from_vulkan_extensions (GXR_APP_OVERLAY,
                                            instance_ext_list,
                                            device_ext_list);

  g_slist_free (instance_ext_list);
  g_slist_free (device_ext_list);

  if (!context)
    {
      g_object_unref (self);
      g_error ("Could not init VR runtime.\n");
      return NULL;
    }

  xrd_client_set_context (XRD_CLIENT (self), context);

  xrd_client_init_input_callbacks (XRD_CLIENT (self));

  XrdDesktopCursor *cursor =
    XRD_DESKTOP_CURSOR (xrd_overlay_desktop_cursor_new (context));
  xrd_client_set_desktop_cursor (XRD_CLIENT (self), cursor);

  return self;
}

static void
xrd_overlay_client_finalize (GObject *gobject)
{
  G_OBJECT_CLASS (xrd_overlay_client_parent_class)->finalize (gobject);
}

static GulkanClient *
_get_gulkan (XrdClient *client)
{
  GxrContext *context = xrd_client_get_gxr_context (client);
  return gxr_context_get_gulkan (context);
}

static void
_init_controller (XrdClient     *client,
                  XrdController *controller)
{
  GxrContext *context = xrd_client_get_gxr_context (client);
  guint64 controller_handle = xrd_controller_get_handle (controller);
  XrdPointer *pointer_ray =
    XRD_POINTER (xrd_overlay_pointer_new (context,
                                          controller_handle));
  if (pointer_ray == NULL)
    {
      g_printerr ("Error: Could not init pointer %lu\n", controller_handle);
      return;
    }
  xrd_controller_set_pointer (controller, pointer_ray);

  XrdPointerTip *pointer_tip =
    XRD_POINTER_TIP (xrd_overlay_pointer_tip_new (context, controller_handle));
  if (pointer_tip == NULL)
    {
      g_printerr ("Error: Could not init pointer tip %lu\n", controller_handle);
      return;
    }

  xrd_pointer_tip_set_active (pointer_tip, FALSE);
  xrd_pointer_tip_show (pointer_tip);

  xrd_controller_set_pointer_tip (controller, pointer_tip);
}

static XrdWindow *
_window_new_from_meters (XrdClient  *client,
                         const char *title,
                         float       width,
                         float       height,
                         float       ppm)
{
  GxrContext *context = xrd_client_get_gxr_context (client);
  return XRD_WINDOW (xrd_overlay_window_new_from_meters (context, title, width,
                                                         height, ppm));
}

static XrdWindow *
_window_new_from_data (XrdClient *client, XrdWindowData *data)
{
  GxrContext *context = xrd_client_get_gxr_context (client);
  return XRD_WINDOW (xrd_overlay_window_new_from_data (context, data));
}

static XrdWindow *
_window_new_from_pixels (XrdClient  *client,
                         const char *title,
                         uint32_t    width,
                         uint32_t    height,
                         float       ppm)
{
  GxrContext *context = xrd_client_get_gxr_context (client);
  return XRD_WINDOW (xrd_overlay_window_new_from_pixels (context, title, width,
                                                         height, ppm));
}

static XrdWindow *
_window_new_from_native (XrdClient   *client,
                         const gchar *title,
                         gpointer     native,
                         uint32_t     width_pixels,
                         uint32_t     height_pixels,
                         float        ppm)
{
  GxrContext *context = xrd_client_get_gxr_context (client);
  return XRD_WINDOW (xrd_overlay_window_new_from_native (context, title, native,
                                                         width_pixels,
                                                         height_pixels,
                                                         ppm));
}

static void
xrd_overlay_client_class_init (XrdOverlayClientClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->finalize = xrd_overlay_client_finalize;

  XrdClientClass *xrd_client_class = XRD_CLIENT_CLASS (klass);
  xrd_client_class->get_gulkan = _get_gulkan;
  xrd_client_class->init_controller = _init_controller;

  xrd_client_class->window_new_from_meters = _window_new_from_meters;
  xrd_client_class->window_new_from_data = _window_new_from_data;
  xrd_client_class->window_new_from_pixels = _window_new_from_pixels;
  xrd_client_class->window_new_from_native = _window_new_from_native;
}
