/*
 * xrdesktop
 * Copyright 2018 Collabora Ltd.
 * Author: Lubosz Sarnecki <lubosz.sarnecki@collabora.com>
 * SPDX-License-Identifier: MIT
 */

#include <glib.h>
#include <gxr.h>

#include "xrd.h"

int
main ()
{
  XrdClient *client = xrd_client_new (XRD_CLIENT_OVERLAY);
  g_assert_nonnull (client);

  XrdWindow *window = xrd_window_new_from_meters (client, "Some window",
                                                  100.0f, 100.0f, 100.0f);

  GValue val = G_VALUE_INIT;
  g_value_init (&val, G_TYPE_STRING);

  g_object_get_property (G_OBJECT (window), "title", &val);

  g_print ("Window name: %s\n", g_value_get_string (&val));

  XrdWindow *xrd_window = XRD_WINDOW (window);

  uint32_t texture_width;
  uint32_t texture_height;

  g_object_get (xrd_window,
                "texture-width", &texture_width,
                "texture-height", &texture_height,
                NULL);

  g_print ("XrdWindow texture dimensions %dx%d\n",
           texture_width, texture_height);

  g_object_unref (window);

  g_object_unref (client);

  return 0;
}
